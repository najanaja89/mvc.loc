{extends file="layouts/main.tpl"}

{block name="content"}

<div class="card card-body">

    <form method="POST">
        <div class="form-group">
            <label for="">User Name</label>
            <input type="text" class="form-control" name="username">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" name="password">
        </div>
        <button class="btn btn-success">Save</button>
    </form>

</div>

{/block}

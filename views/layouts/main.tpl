<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{$title} | {$app.name}</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>
<body>

<div class="container">
    <div class="navbar bg-dark navbar-expand-lg border my-3 rounded">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="/" class="nav-link">
                    Main
                </a>
            </li>
            <li class="nav-item mr-auto">
                <a href="/books" class="nav-link">
                    Books
                </a>
            </li>

        {if $auth == true && $auth_user->isAdmin()}
            <li class="nav-item mr-auto">
                <a href="/users" class="nav-link">
                    Users
                </a>
            </li>
        {/if}

        </ul>

        <ul class="navbar-nav ml-auto">
            {if $auth == true}
                <li class="navbar-text mr-3">
                    <h6 style="color: aliceblue">{$auth_user->username}</h6>
                    {if $auth_user->isAdmin()}
                    <span class="badge badge-primary">administrator</span>
                    {/if}
                </li>
                <li class="nav-item">
                    <a href="/logout" class="nav-link text-danger">
                        Log Out
                    </a>
                </li>
            {else}
                <li class="nav-item">
                    <a href="/login" class="nav-link">
                        Log In
                    </a>
                </li>
            {/if}
        </ul>
    </div>

    {block name="content"}{/block}
</div>

</body>
</html>
